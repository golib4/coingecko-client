package coingecko

import (
	"fmt"
	"gitlab.com/golib4/http-client/http"
)

type PriceData struct {
	MarketData struct {
		CurrentPrice struct {
			USD float64 `json:"usd"`
			EUR float64 `json:"eur"`
			RUB float64 `json:"rub"`
		} `json:"current_price"`
	} `json:"market_data"`
}

type Client struct {
	httpClient *http.Client
}

func NewClient(httpClient *http.Client) Client {
	return Client{
		httpClient: httpClient,
	}
}

type PriceRequest struct {
	Date     string `schema:"date"`
	CoinName string
}

func (c Client) GetPrice(request *PriceRequest) (*PriceData, error) {
	var response PriceData
	httpError, err := c.httpClient.SendGetRequest(http.SchemaRequestData{
		Data: http.RequestData{
			Path: fmt.Sprintf("/api/v3/coins/%s/history", request.CoinName),
		},
		Body: request,
	}, &response)
	if err != nil {
		return nil, fmt.Errorf("error while doing coingecko request: %s", err)
	}
	if httpError != nil {
		return nil, fmt.Errorf("error while doing coingecko request: %s", httpError)
	}

	return &response, nil
}
